package subject;

import java.util.ArrayList;
import observer.Observer;

public class WeatherData implements Subject {
	
	private float temperature, humidity, pressure;
	private ArrayList<Observer> observers;
	
	public WeatherData() {
		observers = new ArrayList<Observer>();
	}
	
	public void measurementsChanged() {
		notifyObservers();
	}
	
	public void setMeasurements(float temperature, float humidity, float pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}
	
	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
	}
	
	@Override
	public void removeObserver(Observer o) {
		int index = observers.indexOf(o);
		
		if(index > 0){
			observers.remove(index);
		}
	}
	
	@Override
	public void notifyObservers() {
		for(Observer o:observers){
			o.update(temperature, humidity, pressure);
		}
	}
}
